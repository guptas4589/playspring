package com.skg.playspring.associations.manytomany;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "teacher")
public class Teacher {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "teacherList")
    private List<Classroom> classroomList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Classroom> getClassroomList() {
        return classroomList;
    }

    public void setClassroomList(List<Classroom> classroomList) {
        this.classroomList = classroomList;
    }
}
