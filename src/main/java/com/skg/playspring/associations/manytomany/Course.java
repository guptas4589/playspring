package com.skg.playspring.associations.manytomany;

import javax.persistence.*;
import java.util.List;

@Entity(name = "course")
public class Course {
    @Id
    @GeneratedValue
    private Long id;


    private Long courseId;

/* When an entity is removed from the @ManyToMany collection, Hibernate simply deletes the joining record in the link table.
    Unfortunately, this operation requires removing all entries associated with a given parent and recreating the ones that are listed
    in the current running persistent context.
*/
/*
For @ManyToMany associations, the REMOVE entity state transition doesn’t make sense to be cascaded because it will
propagate beyond the link table. Since the other side might be referenced by other entities on the parent-side, the
automatic removal might end up in a ConstraintViolationException .
 */
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    private List<Student> studentList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}
