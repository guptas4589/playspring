package com.skg.playspring.associations.manytomany;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "classroom")
public class Classroom {
    @Id
    @GeneratedValue
    private Long id;

    private Long roomCode;

/* When an entity is removed from the @ManyToMany collection, Hibernate simply deletes the joining record in the link table.
    Unfortunately, this operation requires removing all entries associated with a given parent and recreating the ones that are listed
    in the current running persistent context.
*/
/*
For @ManyToMany associations, the REMOVE entity state transition doesn’t make sense to be cascaded because it will
propagate beyond the link table. Since the other side might be referenced by other entities on the parent-side, the
automatic removal might end up in a ConstraintViolationException .
 */
//    @ManyToMany( cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH}) // This is not working
    @ManyToMany( cascade = {CascadeType.ALL})
    private List<Teacher> teacherList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(Long roomCode) {
        this.roomCode = roomCode;
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }
}
