package com.skg.playspring.associations.onetomany;

import javax.persistence.*;
import java.util.List;

@Entity(name = "vehicle")
public class Vehicle {
    @Id
    @GeneratedValue
    private Long id;

/*  If mappedBy not present then joint table is created,
    even if JoinColumn(name = "kald") is present in the other side of
    relation.
 */

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
    private List<Wheel> wheelList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Wheel> getWheelList() {
        return wheelList;
    }

    public void setWheelList(List<Wheel> wheelList) {
        this.wheelList = wheelList;
    }
}
