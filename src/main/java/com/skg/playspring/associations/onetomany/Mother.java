package com.skg.playspring.associations.onetomany;

import javax.persistence.*;
import java.util.List;

/**
 * Created by satish on 6/25/18.
 */
@Entity(name="mother")
public class Mother {

    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @OneToMany
    @JoinColumn(name = "child")
    private List<Child> childList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Child> getChildList() {
        return childList;
    }

    public void setChildList(List<Child> childList) {
        this.childList = childList;
    }
}
