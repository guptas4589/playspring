package com.skg.playspring.associations.manytoone;

import javax.persistence.*;

/**
 * Created by satish on 6/25/18.
 */
@Entity(name="phone")
public class Phone {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name="number")
    private String number;

    @ManyToOne
    @JoinColumn(name = "phone_owner_id", foreignKey = @ForeignKey(name = "PERSON_ID_FK"))
    private Member member;

    public Phone() {
    }

    public Phone(String number) {
    }

    public Long getId() {
        return id;
    }
    public String getNumber() {
        return number;
    }
    public Member getMember() {
        return member;
    }
    public void setMember(Member member) {
        this.member = member;
    }
}
