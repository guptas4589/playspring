package com.skg.playspring.associations.manytoone;

import javax.persistence.*;

@Entity(name="member")
public class Member {
    @Id
    @GeneratedValue
    private Long id;

    String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
