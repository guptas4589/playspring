package com.skg.playspring.associations.onetoone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping("/user")
    public ModelAndView save(Model model) {
        User user = new User();
        user.setName("satish");

        Profile profile = new Profile();
        profile.setPhotoUrl("testUrl");
        user.setProfile(profile);


        model.addAttribute("user", userService.create(user));
        return new ModelAndView("/associations/onetoone/users");
    }
}
