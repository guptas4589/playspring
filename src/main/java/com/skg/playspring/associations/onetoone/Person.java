package com.skg.playspring.associations.onetoone;

import javax.persistence.*;

@Entity(name="person")
public class Person {
    @Id
    @GeneratedValue
    private Long id;

    String name;

    @Embedded
    @AttributeOverrides(
            @AttributeOverride(name="nation", column=@Column(name="country"))
    )
    PersonAddres personAddres;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonAddres getPersonAddres() {
        return personAddres;
    }

    public void setPersonAddres(PersonAddres personAddres) {
        this.personAddres = personAddres;
    }
}
