package com.skg.playspring.associations.onetoone;

import javax.persistence.*;

@Entity(name = "transaction_invoice")
public class TransactionInvoice {
    @Id
    @GeneratedValue
    private Long id;
    private String referenceNumber;

    @OneToOne(mappedBy = "transactionInvoice")
    Transaction transaction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
