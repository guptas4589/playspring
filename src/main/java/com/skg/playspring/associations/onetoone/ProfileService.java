package com.skg.playspring.associations.onetoone;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Service
@Transactional
public class ProfileService {

    @PersistenceContext
    EntityManager entityManager;

    public Profile save(Profile profile) {
        return entityManager.merge(profile);
    }
}
