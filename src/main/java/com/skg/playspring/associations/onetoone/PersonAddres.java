package com.skg.playspring.associations.onetoone;

import javax.persistence.Embeddable;

@Embeddable
public class PersonAddres {
    String street;
    String city;
    String country;
}
