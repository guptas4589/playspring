package com.skg.playspring.associations.onetoone;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "transaction")
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal amount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "transaction_invoice")
    private TransactionInvoice transactionInvoice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionInvoice getTransactionInvoice() {
        return transactionInvoice;
    }

    public void setTransactionInvoice(TransactionInvoice transactionInvoice) {
        this.transactionInvoice = transactionInvoice;
    }
}
