package com.skg.playspring.associations.onetoone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProfileService profileService;

    public User create(User user) {
        Profile profile = profileService.save(user.getProfile());
        user.setProfile(profile);
        return userRepository.save(user);
    }
}
