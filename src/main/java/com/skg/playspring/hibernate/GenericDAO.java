package com.skg.playspring.hibernate;

import org.hibernate.Session;

public class GenericDAO<T> {


    private Session session;

    public GenericDAO(Session session) {
        this.session = session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void update(T entity) {
        session.update(entity);
    }

    public Long insert(T entity) {
        return (Long) session.save(entity);
    }

    public void delete(T entity) {
        session.delete(entity);
    }

    // This call will issue a warning about the unchecked cast,
    // but we know the value returned will be of the right type because
    // we specify the entity (T) class in the call.
    //
    // Note that "get" will return a null if no value with this id fails
    @SuppressWarnings (value="unchecked")
    public T getById(Long id, Class entityClass) {
        return (T) session.get(entityClass, id);
    }

    public void refresh(T entity) {
        session.refresh(entity);
    }
}
