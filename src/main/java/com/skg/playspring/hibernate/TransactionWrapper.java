package com.skg.playspring.hibernate;

import org.hibernate.Session;

public class TransactionWrapper {

    private Session session = HibernateUtil.getSessionFactory().getCurrentSession();

    public Session getSession() {
        return session;
    }
    public void beginTransaction() {
        if(!session.isOpen()) {
            session = HibernateUtil.getSessionFactory().openSession();
        }
        session.beginTransaction();
    }
    public void commit () {
        session.getTransaction().commit();
    }

    public void close() {
        session.close();
    }

    public void flush() {
        session.flush();
    }

}
