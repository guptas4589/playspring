package com.skg.playspring.sender;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SenderService {

    private static Logger logger = LogManager.getLogger();

    @Autowired
    SenderRepository senderRepository;

    public Sender createSender() {
        logger.debug("creating sender");
        return senderRepository.save(new Sender(1L, "satish"));
    }
}
