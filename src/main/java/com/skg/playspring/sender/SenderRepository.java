package com.skg.playspring.sender;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
@Transactional
public class SenderRepository {

    @PersistenceContext
    EntityManager entityManager;

    public Sender findById(Long id) {
        return entityManager.find(Sender.class, id);
    }

    public Sender save(Sender sender) {
        return  entityManager.merge(sender);
    }
}
