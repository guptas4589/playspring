package com.skg.playspring.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SenderController {

    @Autowired
    SenderService senderService;

    @RequestMapping("/sender")
    public String createSender(Model model) {
        model.addAttribute("sender", senderService.createSender());
        return "/sender/sender";
    }
}
