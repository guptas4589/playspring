package com.skg.playspring.associations.manytoone;

import com.skg.playspring.hibernate.GenericDAO;
import com.skg.playspring.hibernate.TransactionWrapper;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *  - Direct equivalence in the relational database as well (e.g. foreign key), and
 *  so it establishes a relationship between a child entity and a parent.
 *
 *  -Many-to-one mappings are always on the owning side of a relationship, so if there is a @JoinColumn to be
 *  found in the relationship that has a many-to-one side, that is where it will be located.
 */
public class ManyToOne {
    /**
     *
     */
    @Test
    public void manyToOneSimpleSucces() {
        Member member = new Member();
        member.setName("Satish");

        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<Member> personDao = new GenericDAO<>( transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        personDao.insert(member);

        Phone phone = new Phone();
        phone.setMember(member);

        GenericDAO<Phone> phoneDao = new GenericDAO<>( transactionWrapper.getSession());
        phoneDao.insert(phone);
        transactionWrapper.commit();

        transactionWrapper = new TransactionWrapper();
//        txnDao = new GenericDAO<>(transaction, transactionWrapper.getSession());
        phoneDao.setSession(transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        phoneDao.refresh(phone);
        assertNotNull(phone.getMember());
        assertEquals(phone.getMember().getId(), member.getId() );
        transactionWrapper.commit();
    }
}
