package com.skg.playspring.associations.onetoone;

import com.skg.playspring.hibernate.GenericDAO;
import com.skg.playspring.hibernate.TransactionWrapper;
import org.hibernate.HibernateException;
import org.junit.Test;
import java.math.BigDecimal;
import static org.junit.Assert.*;

/**
 * Just as with unidirectional one-to-one relationship, bidirectional one-to-one relationship has a single target object reference in the source entity, but additionally target entity has a reference back to the source entity as well.
 * The annotation @OneToOne is used in both source and target classes.
 * We must use 'mappedBy' element with one of the @OneToOne annotations.
 * The value of 'mappedBy' element should be the name of the reference variable used in the other class's back reference.
 * The element 'mappedBy' is needed to specify which side will have the corresponding parent table. Also without 'mappedBy', hibernate will generate the foreign key columns in the both tables. 'mappedBy' must be used to avoid that.
 * The side which has 'mappedBy' specified, will become the target of the relationship and corresponding table will be the parent of the relationship .
 * The side which doesn't have 'mappedBy' element will become the source (owner) and the corresponding table will be the child of the relationship, i.e. it will have the foreign key column.
 * On the owner side, we can also use @JoinColumn, whose one of the purposes is to specify a foreign key column name instead of relying on the default name.
 * From database perspective, there's no difference, between unidirectional and bidirectional one-to-one relationships, because we can always use the queries to get data in the other direction.
 */
public class OneToOneBidirectionalTest {

    @Test
    public void settingOnlyOwnerSideAssociationDoesNotAssignAssociationAtChildEndWithoutReattachment() {
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal("453"));
        TransactionInvoice transactionInvoice = new TransactionInvoice();
        transaction.setTransactionInvoice(transactionInvoice);

        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<Transaction> txnDao = new GenericDAO<>( transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        txnDao.insert(transaction);
        transactionWrapper.commit();

        assertNotNull(transaction.getTransactionInvoice());
        assertNull(transactionInvoice.getTransaction());

        transactionWrapper = new TransactionWrapper();
//        txnDao = new GenericDAO<>(transaction, transactionWrapper.getSession());
        txnDao.setSession(transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        txnDao.refresh(transaction);
        assertNotNull(transaction.getTransactionInvoice());
        assertNotNull(transactionInvoice.getTransaction());
        transactionWrapper.commit();

    }

    @Test
    public void settingOnlyChildSideAssociationDoesNotSaveChild() {
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal("567"));
        TransactionInvoice transactionInvoice = new TransactionInvoice();
        transactionInvoice.setTransaction(transaction);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Transaction> txnDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        txnDao.insert(transaction);

        assertNotNull(transactionInvoice.getTransaction());
        assertNull(transaction.getTransactionInvoice());

        txnWrapper.commit();


        txnWrapper = new TransactionWrapper();
        txnDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        txnDao.refresh(transaction);
        assertNotNull(transactionInvoice.getTransaction());
        assertNull(transactionInvoice.getTransaction().getTransactionInvoice());
        txnWrapper.commit();
    }

    /**
     *
     */
    @Test
    public void ifUniquenessConstraintNotExplicitWithJoinColumnThenErrorWhenModelsAreReloaded() {
        TransactionInvoice transactionInvoice = new TransactionInvoice();

        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal("123"));
        transaction.setTransactionInvoice(transactionInvoice);

        Transaction transaction1 = new Transaction();
        transaction1.setAmount(new BigDecimal("234"));
        transaction1.setTransactionInvoice(transactionInvoice);

        transactionInvoice.setTransaction(transaction);
        transactionInvoice.setReferenceNumber("a1");
        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Transaction> txnDao = new GenericDAO<>( txnWrapper.getSession());
        txnWrapper.beginTransaction();
        txnDao.insert(transaction);

        txnDao.insert(transaction1);
        txnWrapper.commit();


        try {
            txnWrapper = new TransactionWrapper();
            txnDao.setSession(txnWrapper.getSession());
            txnWrapper.beginTransaction();

            txnDao.refresh(transaction);
            txnDao.refresh(transaction1);
            fail("Expected an org.hibernate.ConstraintViolationException to be thrown");
        } catch (Exception e) {
            assertEquals(e.getCause().getClass(), HibernateException.class);
            assertTrue(e.getMessage().contains("More than one row with the given identifier was found"));
        } finally {
            txnWrapper.close();
            txnWrapper = new TransactionWrapper();
            txnDao.setSession(txnWrapper.getSession());
            txnWrapper.beginTransaction();
            txnDao.delete(transaction1);
            txnWrapper.commit();
        }
    }
}
