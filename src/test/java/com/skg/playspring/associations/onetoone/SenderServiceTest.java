package com.skg.playspring.associations.onetoone;

import com.skg.playspring.sender.Sender;
import com.skg.playspring.sender.SenderRepository;
import com.skg.playspring.sender.SenderService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SenderServiceTest {

    @InjectMocks
    SenderService senderService;

    @Mock
    SenderRepository senderRepository;

//    this is a dumb test
    @Test
    public void testSenderSave() {
        Sender sender = new Sender();
        sender.setName("satish");
        when(senderRepository.save(any(Sender.class))).thenReturn(sender);
        Assert.assertEquals("satish", senderService.createSender().getName());
    }
}
