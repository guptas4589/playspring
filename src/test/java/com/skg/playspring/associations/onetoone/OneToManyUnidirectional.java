package com.skg.playspring.associations.onetoone;

import com.skg.playspring.associations.onetomany.Child;
import com.skg.playspring.associations.onetomany.Machine;
import com.skg.playspring.associations.onetomany.Mother;
import com.skg.playspring.associations.onetomany.Part;
import com.skg.playspring.hibernate.GenericDAO;
import com.skg.playspring.hibernate.TransactionWrapper;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.TransientObjectException;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;

/**
 *
 */
public class OneToManyUnidirectional {
    /**
     *  https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/
     *
     */
    @Test
    public void oneToManyUnidirectionalWithoutJoinColumn() { // Creates only three tables
        Part part1 = new Part();
        part1.setName("PartA1");
        Part part2 = new Part();
        part1.setName("PartA2");

        Machine machine = new Machine();
        machine.setName("MachineA1");
        ArrayList<Part> arrayList = new ArrayList<>();
        arrayList.add(part1);
        arrayList.add(part2);
        machine.setPartList(arrayList);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Machine> machineDAO = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        machineDAO.insert(machine);
        txnWrapper.commit();

        txnWrapper = new TransactionWrapper();
        machineDAO.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        machineDAO.refresh(machine);
        txnWrapper.commit();
        assertNotNull(machine.getPartList().get(0).getId());
        assertNotNull(machine.getPartList().get(1).getId());
    }

    @Test
    public void oneToManyUnidirectionalWithJoinColumn() { // Creates only two tables
        Child child1 = new Child();
        child1.setName("ChildA1");
        Child child2 = new Child();
        child2.setName("ChildA2");

        Mother mother = new Mother();
        mother.setName("MotherA1");
        ArrayList<Child> arrayList = new ArrayList<>();
        arrayList.add(child1);
        arrayList.add(child2);
        mother.setChildList(arrayList);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Mother> motherDao = new GenericDAO<>(txnWrapper.getSession());
        GenericDAO<Child> childDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        childDao.insert(child1);
        childDao.insert(child2);
        motherDao.insert(mother);
        txnWrapper.commit();

        txnWrapper = new TransactionWrapper();
        motherDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        motherDao.refresh(mother);
        Hibernate.initialize(mother.getChildList()); // to get around lazy fetch
        txnWrapper.commit();


        assertNotNull(mother.getChildList().get(0).getId());
        assertNotNull(mother.getChildList().get(1).getId());
    }

    @Test
    public void cannotSaveParentWithoutSavingChildWithNoCascadeSpecified() { // Creates only two tables
        Child child1 = new Child();
        child1.setName("ChildA1");

        Mother mother = new Mother();
        mother.setName("MotherA1");
        ArrayList<Child> arrayList = new ArrayList<>();
        arrayList.add(child1);
        mother.setChildList(arrayList);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Mother> motherDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();

        try {
            motherDao.insert(mother);
            txnWrapper.commit();
            fail("Expected an org.hibernate.TransientObjectException to be thrown");
        } catch (Exception e) {
            assertEquals(e.getCause().getClass(), TransientObjectException.class);
            assertTrue(e.getMessage().contains("object references an unsaved transient instance - save the transient instance before flushing"));
        } finally {
            txnWrapper.close();
        }
    }
}
