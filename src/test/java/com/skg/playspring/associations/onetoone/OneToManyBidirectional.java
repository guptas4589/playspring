package com.skg.playspring.associations.onetoone;

import com.skg.playspring.associations.onetomany.Vehicle;
import com.skg.playspring.associations.onetomany.Wheel;
import com.skg.playspring.hibernate.GenericDAO;
import com.skg.playspring.hibernate.TransactionWrapper;
import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class OneToManyBidirectional {
    /**
     *
     */
    @Test
    public void oneToManyUnidirectionalBasic() {
        Wheel wheel1 = new Wheel();
        Wheel wheel2 = new Wheel();

        Vehicle vehicle = new Vehicle();
        ArrayList<Wheel> arrayList = new ArrayList<>();
        arrayList.add(wheel1);
        arrayList.add(wheel2);
        vehicle.setWheelList(arrayList);
        wheel1.setVehicle(vehicle);
        wheel2.setVehicle(vehicle);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Vehicle> vehicleDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        vehicleDao.insert(vehicle);
        txnWrapper.commit();

        txnWrapper = new TransactionWrapper();
        vehicleDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        vehicleDao.refresh(vehicle);

        GenericDAO<Wheel> wheelDao = new GenericDAO<>(txnWrapper.getSession());
        wheelDao.refresh(wheel1);
        wheelDao.refresh(wheel2);
        txnWrapper.commit();

        assertNotNull(vehicle.getWheelList().get(0).getId());
        assertNotNull(vehicle.getWheelList().get(1).getId());

        assertNotNull(wheel1.getVehicle());
        assertNotNull(wheel2.getVehicle());

        assertTrue(wheel1.getVehicle().getId().equals(vehicle.getId()));
        assertTrue(wheel2.getVehicle().getId().equals(vehicle.getId()));
    }

    @Test
    public void settingOnlyManySideDoesNotSaveAssociationAtBothEnds() {
        Wheel wheel1 = new Wheel();
        Wheel wheel2 = new Wheel();

        Vehicle vehicle = new Vehicle();
        ArrayList<Wheel> arrayList = new ArrayList<>();
        arrayList.add(wheel1);
        arrayList.add(wheel2);
        vehicle.setWheelList(arrayList);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Vehicle> vehicleDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        vehicleDao.insert(vehicle);
        txnWrapper.commit();

        txnWrapper = new TransactionWrapper();
        vehicleDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        vehicleDao.refresh(vehicle);

        GenericDAO<Wheel> wheelDao = new GenericDAO<>(txnWrapper.getSession());
        wheelDao.refresh(wheel1);
        wheelDao.refresh(wheel2);
        txnWrapper.commit();

        assertTrue(vehicle.getWheelList().size() == 0);
        assertTrue(vehicle.getWheelList().size() == 0);

        assertNull(wheel1.getVehicle());
        assertNull(wheel2.getVehicle());

    }

    @Test
    public void settingAssociationAtChildSideDoesNotReflectAtParent() {
        Wheel wheel1 = new Wheel();
        Wheel wheel2 = new Wheel();

        Vehicle vehicle = new Vehicle();
        wheel1.setVehicle(vehicle);
        wheel2.setVehicle(vehicle);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Vehicle> vehicleDao = new GenericDAO<>(txnWrapper.getSession());
        GenericDAO<Wheel> wheelDao = new GenericDAO<>(txnWrapper.getSession());

        txnWrapper.beginTransaction();
        vehicleDao.insert(vehicle);
        wheelDao.insert(wheel1);
        wheelDao.insert(wheel2);
        txnWrapper.commit();

        assertNull(vehicle.getWheelList());
    }

    @Test
    public void settingAssociationAtChildSideDoesReflectAssociationAtParentAfterRefresh() {
        Wheel wheel1 = new Wheel();
        Wheel wheel2 = new Wheel();

        Vehicle vehicle = new Vehicle();
        wheel1.setVehicle(vehicle);
        wheel2.setVehicle(vehicle);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Vehicle> vehicleDao = new GenericDAO<>(txnWrapper.getSession());
        GenericDAO<Wheel> wheelDao = new GenericDAO<>(txnWrapper.getSession());

        txnWrapper.beginTransaction();
        vehicleDao.insert(vehicle);
        wheelDao.insert(wheel1);
        wheelDao.insert(wheel2);
        txnWrapper.commit();

        assertNull(vehicle.getWheelList());

        txnWrapper = new TransactionWrapper();
        vehicleDao.setSession(txnWrapper.getSession());
        wheelDao.setSession(txnWrapper.getSession());

        txnWrapper.beginTransaction();
        vehicleDao.refresh(vehicle);
        wheelDao.refresh(wheel1);
        wheelDao.refresh(wheel2);
        txnWrapper.commit();

        assertTrue(vehicle.getWheelList().size() > 0);
        assertTrue(vehicle.getWheelList().size() > 0);

        assertNotNull(wheel1.getVehicle());
        assertNotNull(wheel2.getVehicle());
    }
}
