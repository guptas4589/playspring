package com.skg.playspring.associations.onetoone;

import com.skg.playspring.hibernate.GenericDAO;
import com.skg.playspring.hibernate.TransactionWrapper;
import org.hibernate.TransientObjectException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import static org.junit.Assert.*;

public class OneToOneTest {

    @Test
    public void savingParentOnlyWithoutCascadeEnabledThrowsException() {
        User user = new User();
        user.setName("satish");

        Profile profile = new Profile();
        profile.setPhotoUrl("testPhotoUrl");
        user.setProfile(profile);

        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<User> genericDAO = new GenericDAO<>( transactionWrapper.getSession());

        try {
            transactionWrapper.beginTransaction();
            genericDAO.insert(user);
            transactionWrapper.commit();
            fail("Expected an org.hibernate.TransientObjectException to be thrown");
        } catch (Exception e) {
            assertEquals(e.getCause().getClass(), TransientObjectException.class);
            assertTrue(e.getMessage().contains("object references an unsaved transient instance"));
        } finally {
            transactionWrapper.close();
        }
    }

    /**
     * In one to one unidirectional relationship @JoinColumn(unique=true) in parent associations
     * maintains the uniqueness constraint is not enforced, which makes the association a one to one association.
     */
    @Test
    public void oneToOneDoesUniquenessConstraint() {
        User user = new User();
        user.setName("Satish");
        User user2 = new User();
        user2.setName("Manish");

        Profile profile = new Profile();
        user.setProfile(profile);
        user2.setProfile(profile);

        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<Profile> profileDao = new GenericDAO<>( transactionWrapper.getSession());
        GenericDAO<User> userDao = new GenericDAO<>(transactionWrapper.getSession());
        try {
            transactionWrapper.beginTransaction();
            profileDao.insert(profile);
            userDao.insert(user);
            userDao.insert(user2);
            transactionWrapper.commit();
            fail("Expected an org.hibernate.ConstraintViolationException to be thrown");
        } catch (Exception e) {
            assertEquals(e.getCause().getClass(), ConstraintViolationException.class);
        } finally {
            transactionWrapper.close();
        }
    }

    /**
     * In one to one unidirectional relationship @JoinColumn(nullable: false) is required
     * if there should be relationship from owining side
     */
    @Test
    public void oneToOneDoesNotMandateRelationshipWithoutExplicitNullableFalseConstraint() {
        User user = new User();
        user.setName("Satish");

        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<User> userDao = new GenericDAO<>(transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        userDao.insert(user);
        transactionWrapper.commit();

        transactionWrapper = new TransactionWrapper();
        transactionWrapper.beginTransaction();
        userDao = new GenericDAO<>(transactionWrapper.getSession());
        assertNotNull(userDao.getById(user.getId(), User.class));
        transactionWrapper.commit();
    }

//    TODO: implement One to One with mandatory child using @JoinColumn(nullable=true) in child reference in parent.
//    TODO: implement embedded approach for one to one association Person entity and PersonAddress class already created.
}
