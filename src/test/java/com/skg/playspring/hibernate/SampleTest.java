package com.skg.playspring.hibernate;

import com.skg.playspring.sender.Sender;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class SampleTest {

    @Test
    public void sampleTest() {
        Sender sender = new Sender();
        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<Sender> genericDAO = new GenericDAO<>(transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        sender.setName("satish1");
        assertNull(sender.getId());
        genericDAO.insert(sender);
        assertNotNull(sender.getId());
        transactionWrapper.commit();
    }
}
