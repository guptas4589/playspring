package com.skg.playspring.asscociations.manytomany;


import com.skg.playspring.associations.manytomany.Classroom;
import com.skg.playspring.associations.manytomany.Course;
import com.skg.playspring.associations.manytomany.Student;
import com.skg.playspring.associations.manytomany.Teacher;
import com.skg.playspring.hibernate.GenericDAO;
import com.skg.playspring.hibernate.TransactionWrapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ManyToManyBidirectional {

    @Test
    public void testManyToManyBasic() {
        Classroom classroom1 = new Classroom();
        classroom1.setRoomCode(111L);
        Classroom classroom2 = new Classroom();
        classroom2.setRoomCode(222L);

        Teacher teacher1 = new Teacher();
        teacher1.setName("aaaa");
        Teacher teacher2 = new Teacher();
        teacher2.setName("bbb");

        classroom1.getTeacherList().add(teacher1);

        classroom1.getTeacherList().add(teacher2);

        classroom2.getTeacherList().add(teacher1);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Classroom> classroomDao = new GenericDAO<>(txnWrapper.getSession());

        txnWrapper.beginTransaction();
        classroomDao.insert(classroom1);
        classroomDao.insert(classroom2);
        txnWrapper.commit();

        txnWrapper = new TransactionWrapper();
        classroomDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        classroomDao.refresh(classroom1);
        classroomDao.refresh(classroom2);

        txnWrapper = new TransactionWrapper();
        GenericDAO<Teacher> teacherDao = new GenericDAO<>(txnWrapper.getSession());
        teacherDao.setSession(txnWrapper.getSession());
        teacherDao.refresh(teacher1);
        teacherDao.refresh(teacher2);
        txnWrapper.commit();

        assertNotNull(classroom1.getTeacherList());
        assertNotNull(classroom2.getTeacherList());
        assertTrue(classroom1.getTeacherList().size() > 0);
        assertTrue(classroom2.getTeacherList().size() > 0);
        assertTrue(classroom1.getTeacherList().get(0).getId() == teacher1.getId() || classroom1.getTeacherList().get(1).getId() == teacher1.getId() );
        assertTrue(classroom1.getTeacherList().get(0).getId() == teacher2.getId() || classroom1.getTeacherList().get(1).getId() == teacher2.getId() );
        assertTrue(classroom2.getTeacherList().get(0).getId() == teacher1.getId() || classroom1.getTeacherList().get(1).getId() == teacher1.getId() );
    }

//    @Test
//    public void settingCollectionDirectlyDoesNotCascadeSave() {
//        Student student1 = new Student();
//        student1.setName("stud1");
//        Student student2 = new Student();
//        student2.setName("stud2");
//
//        List<Student> studentList = new ArrayList<>();
//        Course course = new Course();
//        course.setCourseId(123L);
//        course.setStudentList(studentList);
//
//        TransactionWrapper txnWrapper = new TransactionWrapper();
//        GenericDAO<Course> courseDao = new GenericDAO<>(txnWrapper.getSession());
//        txnWrapper.beginTransaction();
//        courseDao.insert(course);
//        txnWrapper.commit();
//
//        assertNotNull(course.getStudentList());
//        assertNotNull(course.getStudentList().size() > 0);
//
//        txnWrapper = new TransactionWrapper();
//        courseDao.setSession(txnWrapper.getSession());
//        txnWrapper.beginTransaction();
//        courseDao.refresh(course);
//        txnWrapper.commit();
//
//        assertTrue(course.getStudentList().size() == 0);
//        assertNull(student1.getId());
//        assertNull(student2.getId());
//    }

//    Test removal with cascading and with no cascading;
/* When an entity is removed from the @ManyToMany collection, Hibernate simply deletes the joining record in the link table.
Unfortunately, this operation requires removing all entries associated with a given parent and recreating the ones that are listed
in the current running persistent context.
*/
/*
For @ManyToMany associations, the REMOVE entity state transition doesn’t make sense to be cascaded because it will
propagate beyond the link table. Since the other side might be referenced by other entities on the parent-side, the
automatic removal might end up in a ConstraintViolationException .
 */

}
