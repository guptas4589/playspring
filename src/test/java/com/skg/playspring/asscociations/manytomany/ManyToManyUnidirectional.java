package com.skg.playspring.asscociations.manytomany;


import com.skg.playspring.associations.manytomany.Course;
import com.skg.playspring.associations.manytomany.Student;
import com.skg.playspring.hibernate.GenericDAO;
import com.skg.playspring.hibernate.TransactionWrapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ManyToManyUnidirectional {

    @Test
    public void testManyToManyBasic() {
        Student student1 = new Student();
        student1.setName("stud1");
        Student student2 = new Student();
        student2.setName("stud2");

        List<Student> studentList = new ArrayList<>();
        Course course = new Course();
        course.setCourseId(123L);
        course.setStudentList(studentList);

        course.getStudentList().add(student1);
        course.getStudentList().add(student2);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Course> courseDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        courseDao.insert(course);
        txnWrapper.commit();

        txnWrapper = new TransactionWrapper();
        courseDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        courseDao.refresh(course);
        txnWrapper.commit();

        assertNotNull(course.getStudentList());
        assertTrue(course.getStudentList().size() > 0);
        assertTrue(course.getStudentList().get(0).getId() == student1.getId() || course.getStudentList().get(1).getId() == student1.getId() );
        assertTrue(course.getStudentList().get(0).getId() == student2.getId() || course.getStudentList().get(1).getId() == student2.getId() );
    }

    @Test
    public void settingCollectionDirectlyDoesNotCascadeSave() {
        Student student1 = new Student();
        student1.setName("stud1");
        Student student2 = new Student();
        student2.setName("stud2");

        List<Student> studentList = new ArrayList<>();
        Course course = new Course();
        course.setCourseId(123L);
        course.setStudentList(studentList);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Course> courseDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        courseDao.insert(course);
        txnWrapper.commit();

        assertNotNull(course.getStudentList());
        assertNotNull(course.getStudentList().size() > 0);

        txnWrapper = new TransactionWrapper();
        courseDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        courseDao.refresh(course);
        txnWrapper.commit();

        assertTrue(course.getStudentList().size() == 0);
        assertNull(student1.getId());
        assertNull(student2.getId());
    }

//    Test removal with cascading and with no cascading;
/* When an entity is removed from the @ManyToMany collection, Hibernate simply deletes the joining record in the link table.
Unfortunately, this operation requires removing all entries associated with a given parent and recreating the ones that are listed
in the current running persistent context.
*/
/*
For @ManyToMany associations, the REMOVE entity state transition doesn’t make sense to be cascaded because it will
propagate beyond the link table. Since the other side might be referenced by other entities on the parent-side, the
automatic removal might end up in a ConstraintViolationException .
 */

}
